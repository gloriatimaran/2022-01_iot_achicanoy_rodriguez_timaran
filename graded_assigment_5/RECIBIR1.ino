//INTEGRANTES: Camila Achicanoy Caicedo
//             Ever Mauricio Rodriguez 
//             Gloria Daniela Timarán 

//Include Libraries
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

//create an RF24 object
RF24 radio(12, 14, 26, 25, 27);

//address through which two modules communicate.
const byte address[6] = "00010";

float datasensors[2];

typedef struct{
  float sender;
  float datasensor;
  int counter;
}Data;

void setup()
{
  while (!Serial);
    Serial.begin(9600);
  
    radio.begin();
  
  //set the address
  radio.openReadingPipe(0, address);
  
  //Set module as receiver
  radio.startListening();
}

void loop()
{
    /* create data to send */
  //Data data;
  /* sender 1 has id is 1 */
  //data.sender = 1;
  //Read the data if available in buffer
  if (radio.available())
  {
    //float text;
    radio.read(&datasensors, sizeof(datasensors));
    Serial.print("Sensor 1 = ");
    Serial.println(datasensors[0]);
    //Serial.print(data.sender);
    Serial.print("Sensor 2 = ");
    Serial.println(datasensors[1]);

    
    //radio.read(&data, sizeof(data.datasensor));
    //Serial.println("\t\t\t\t POTENCIOMETRO - Adding "+String(data.datasensor));
  }
}
