//INTEGRANTES: Camila Achicanoy Caicedo
//             Ever Mauricio Rodriguez 
//             Gloria Daniela Timarán 



//Include Libraries
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include <DHT.h>
#define DHTPin 21
#define DHTTipo DHT11
DHT dht(DHTPin,DHTTipo);
/* structure that hold data*/

//create an RF24 object
//create an RF24 object
RF24 radio(12, 14, 26, 25, 27);

//address through which two modules communicate.
const byte address[6] = "00010";
  float datasensors[2];

typedef struct{
  float sender;
  float datasensor;
  
}Data;

/* this variable hold queue handle */
xQueueHandle xQueue;
SemaphoreHandle_t xBinarySemaphore;

void setup()
{

  Serial.begin(9600);
  radio.begin();
  dht.begin();
  
  //set the address
  radio.openWritingPipe(address);
  
  //Set module as transmitter
  radio.stopListening();

  xBinarySemaphore = xSemaphoreCreateBinary();
  /* create the queue which size can contains 5 elements of Data */
  xQueue = xQueueCreate(5, sizeof(datasensors));
  xTaskCreatePinnedToCore(
      sendTask1,           /* Task function. */
      "sendTask1",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      sendTask2,           /* Task function. */
      "sendTask2",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,1);                    /* Task handle to keep track of created task */ 
}
//////////////////////////////////////////////////////////////////////////////
void loop()
{
  //Send message to receiver
  //const char text[] = "Hello World";
  //Serial.println("send ...");
  //radio.write(&text, sizeof(text));
  
  //delay(1000);
}
///////////////////////////////////////////////////////////////////////////////
void sendTask1( void * parameter )//potenciometro
{
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data1;
  /* sender 1 has id is 1 */
  data1.sender = 1;

  for(;;){
    int potValor = 0;
    const int portPin = 33;
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    potValor = analogRead(portPin);
    datasensors[0]=((potValor*100)/4095);
    //Serial.println("sendTask1 is sending data");
    /* send data to front of the queue */
    Serial.println("\t\t\t\t POTENCIOMETRO - Adding "+String(datasensors[0]));
    
    

    
    
    xStatus = xQueueSendToBack( xQueue, &datasensors, xTicksToWait );
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
/* this task is similar to sendTask1 */
void sendTask2( void * parameter )  // temperatura - humedad
{
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data2;
  data2.sender = 2;

  xSemaphoreGive(xBinarySemaphore);
  
  for(;;){
    //Lecturas del sensor
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    datasensors[1]= dht.readHumidity();
    //Serial.println("sendTask2 is sending data");
    Serial.println("\t\t\t\t HUMEDAD - Adding "+String(datasensors[1]));
    //float text1 = data.datasensor;
    //radio.write(&text1, sizeof(text1));
    
    
    //data.datasensor = dht.readTemperature ();
    //Serial.println("\t\t\t\t TEMPERATURA - Adding "+String(data.datasensor));
    //float text2 = data.datasensor2;
    //radio.write(&text2, sizeof(text2));
    
    xStatus = xQueueSendToBack( xQueue, &datasensors, xTicksToWait );

   /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
void receiveTask( void * parameter )
{
  
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  for(;;){
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &datasensors, xTicksToWait );
    /* check whether receiving is ok or not */

    if(xStatus == pdPASS){
      /* print the data to terminal */
      Serial.print("Visualizing data: ");
      Serial.print("Sensor 1 = ");
      Serial.print(datasensors[0]);
      //Serial.print(data.sender);
       Serial.print("Sensor 2 = ");
      Serial.print(datasensors[1]);
      radio.write(&datasensors, sizeof(datasensors));
    }
  }
  vTaskDelete( NULL );
}
