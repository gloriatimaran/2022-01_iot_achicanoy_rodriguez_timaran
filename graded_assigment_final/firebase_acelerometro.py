import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import random
import datetime

#ace
import smbus  # import SMBus module of I2C
from time import sleep  # import

#ace
PWR_MGMT_1 = 0x6B
SMPLRT_DIV = 0x19
CONFIG = 0x1A
GYRO_CONFIG = 0x1B
INT_ENABLE = 0x38
ACCEL_XOUT_H = 0x3B
ACCEL_YOUT_H = 0x3D
ACCEL_ZOUT_H = 0x3F
GYRO_XOUT_H = 0x43
GYRO_YOUT_H = 0x45
GYRO_ZOUT_H = 0x47

def MPU_Init():
    # write to sample rate register
    bus.write_byte_data(Device_Address, SMPLRT_DIV, 7)

    # Write to power management register
    bus.write_byte_data(Device_Address, PWR_MGMT_1, 1)

    # Write to Configuration register
    bus.write_byte_data(Device_Address, CONFIG, 0)

    # Write to Gyro configuration register
    bus.write_byte_data(Device_Address, GYRO_CONFIG, 24)

    # Write to interrupt enable register
    bus.write_byte_data(Device_Address, INT_ENABLE, 1)


def read_raw_data(addr):
    # Accelero and Gyro value are 16-bit
    high = bus.read_byte_data(Device_Address, addr)
    low = bus.read_byte_data(Device_Address, addr + 1)

    # concatenate higher and lower value
    value = ((high << 8) | low)

    # to get signed value from mpu6050
    if (value > 32768):
        value = value - 65536
    return value

bus = smbus.SMBus(1)  # or bus = smbus.SMBus(0) for older version boards
Device_Address = 0x68  # MPU6050 device address

MPU_Init()

def generate_time(initial_time):
    final_time = datetime.datetime.now()
    delta = final_time - initial_time
    return delta.total_seconds()


initial_time = datetime.datetime.now()

try:
    app = firebase_admin.get_app()
except ValueError as e:
    # Fetch the service account key JSON file contents
    cred = credentials.Certificate('adminsdcard.json')
    # Initialize the app with a service account, granting admin privileges
    firebase_admin.initialize_app(cred, {'databaseURL': 'https://fir-test-f7ec4-default-rtdb.firebaseio.com/'})

###################################################################################################
ref = db.reference("sensor1/")
ref.set({'accx': {'data': 0.0, 'time': 0.0}, 'accy': {'data': 0.0, 'time': 0.0}, 'accz': {'data': 0.0, 'time': 0.0}})
# update sensors
ref = db.reference('sensor1')

###############################################################################################
ref1 = db.reference("sensor2/")
ref1.set({'gyrox': {'data': 0.0, 'time': 0.0}, 'gyroy': {'data': 0.0, 'time': 0.0}, 'gyroz': {'data': 0.0, 'time': 0.0}})
# update sensors
ref1 = db.reference('sensor2')

#################################################################################################

trigger = True

while True:

    try:
        acc_x = read_raw_data(ACCEL_XOUT_H)
        acc_y = read_raw_data(ACCEL_YOUT_H)
        acc_z = read_raw_data(ACCEL_ZOUT_H)

        # Read Gyroscope raw value
        gyro_x = read_raw_data(GYRO_XOUT_H)
        gyro_y = read_raw_data(GYRO_YOUT_H)
        gyro_z = read_raw_data(GYRO_ZOUT_H)

        # Full scale range +/- 250 degree/C as per sensitivity scale factor
        Ax = acc_x / 16384.0
        Ay = acc_y / 16384.0
        Az = acc_z / 16384.0

        Gx = gyro_x / 131.0
        Gy = gyro_y / 131.0
        Gz = gyro_z / 131.0

        # accx
        accx = Ax
        time1 = generate_time(initial_time)

        # accy
        accy = Ay
        time2 = generate_time(initial_time)

        # accz
        accz = Az
        time3 = generate_time(initial_time)

        # gyrox
        gyrox = Gx
        time4 = generate_time(initial_time)

        # gyroy
        gyroy = Gy
        time5 = generate_time(initial_time)

        # gyroz
        gyroz = Gz
        time6 = generate_time(initial_time)

        ref.update({
            'accx/data': accx,
            'accx/time': time1,

            'accy/data': accy,
            'accy/time': time2,

            'accz/data': accz,
            'accz/time': time3,
        })

        ref1.update({

            'gyrox/data': gyrox,
            'gyrox/time': time4,

            'gyroy/data': gyroy,
            'gyroy/time': time5,

            'gyroz/data': gyroz,
            'gyroz/time': time6,
        })

        print(ref.get(),ref1.get())

    except KeyboardInterrupt:
        break