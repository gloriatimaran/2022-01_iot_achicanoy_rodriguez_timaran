//GRUPO 6 
//INTEGRANTES: Camila Achicanoy Caicedo
//             Ever Mauricio Rodriguez 
//             Gloria Daniela Timarán 

#include <DHT.h>
#define DHTPin 21
#define DHTTipo DHT11
DHT dht(DHTPin,DHTTipo);
/* structure that hold data*/
typedef struct{
  float sender;
  float datasensor;
  float datasensor2;
  int counter;
  float potValor2;
}Data;

/* this variable hold queue handle */
xQueueHandle xQueue;
SemaphoreHandle_t xBinarySemaphore;
void setup() {

  Serial.begin(115200);
  dht.begin();
  //Serial.println("Bluetooth Device is Ready to Pair");

  xBinarySemaphore = xSemaphoreCreateBinary();
  /* create the queue which size can contains 5 elements of Data */
  xQueue = xQueueCreate(5, sizeof(Data));
  xTaskCreatePinnedToCore(
      sendTask1,           /* Task function. */
      "sendTask1",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      sendTask2,           /* Task function. */
      "sendTask2",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,1);                    /* Task handle to keep track of created task */
}

void loop() {

}

void sendTask1( void * parameter )//potenciometro
{
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data;
  /* sender 1 has id is 1 */
  data.sender = 1;

  for(;;){
     int potValor = 0;
     const int portPin = 33;
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    potValor = analogRead(portPin);
    data.datasensor=((potValor*100)/4095);
    //Serial.println("sendTask1 is sending data");
    /* send data to front of the queue */
    Serial.println("\t\t\t\t POTENCIOMETRO - Adding "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /* check whether sending is ok or not */
    if( xStatus == pdPASS ) {
      /* increase counter of sender 1 */
      data.counter = data.counter + 1;
    }
    /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
/* this task is similar to sendTask1 */
void sendTask2( void * parameter )  // temperatura - humedad
{
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  data.sender = 2;

  xSemaphoreGive(xBinarySemaphore);
  
  for(;;){
    //Lecturas del sensor
    float t ;
    float h ;
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    data.datasensor = dht.readHumidity();
    //Serial.println("sendTask2 is sending data");
    Serial.println("\t\t\t\t HUMEDAD - Adding "+String(data.datasensor));
    
    data.datasensor2 = dht.readTemperature ();
    Serial.println("\t\t\t\t TEMPERATURA - Adding "+String(data.datasensor2));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    if( xStatus == pdPASS ) {
      data.counter = data.counter + 1;
    }
   /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
void receiveTask( void * parameter )
{
  
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  for(;;){
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &data, xTicksToWait );
    /* check whether receiving is ok or not */

    if(xStatus == pdPASS){
      /* print the data to terminal */
      Serial.print("Visualizing data: ");
      Serial.print("Sensor = ");
      Serial.print(data.sender);
      Serial.print(" with data = ");
      Serial.println(data.datasensor);
    }
  }
  vTaskDelete( NULL );
}
