//INTEGRANTES: Camila Achicanoy Caicedo
//             Ever Mauricio Rodriguez 
//             Gloria Daniela Timarán 

#include <DHT.h>
#define DHTPin 21
#define DHTTipo DHT11
DHT dht(DHTPin,DHTTipo);
const int Trigger = 34;
const int Echo = 35;

#include <WiFi.h>
#include <PubSubClient.h>
const char* ssid = "CamilaAC";
const char* password = "CamilaAC";
//const char* mqttServer = "192.168.117.22";// broker.emqx.io
const char* mqttServer = "broker.emqx.io";
const int mqttPort = 1883;
const char* mqttUser = "emqx";
const char* mqttPassword = "public";
char valores0[50];
char valores1[50];
WiFiClient espClient;
PubSubClient client(espClient);
/* structure that hold data*/

//address through which two modules communicate.
const byte address[6] = "00010";
  float datasensors[2];

typedef struct{
  float sender;
  float datasensor;
  
}Data;

/* this variable hold queue handle */
xQueueHandle xQueue;
SemaphoreHandle_t xBinarySemaphore;

void setup()
{

  Serial.begin(9600);
  //radio.begin();
  dht.begin();
  pinMode(Trigger, OUTPUT);
  pinMode(Echo,INPUT);
  digitalWrite(Trigger, LOW);
    WiFi.begin(ssid, password);
  Serial.println("...................................");

  Serial.print("Connecting to WiFi.");
  while (WiFi.status() != WL_CONNECTED)
       {  delay(500);
          Serial.print(".") ;
       }
  Serial.println("Connected to the WiFi network");

  client.setServer(mqttServer, mqttPort);
  while (!client.connected())
  {      Serial.println("Connecting to MQTT...");
       if (client.connect("ESP32Client", mqttUser, mqttPassword ))
           Serial.println("connected");
       else
       {   Serial.print("failed with state ");
           Serial.print(client.state());
           delay(2000);
       }
}
  
  //set the address
  //radio.openWritingPipe(address);
  
  //Set module as transmitter
  //radio.stopListening();

  xBinarySemaphore = xSemaphoreCreateBinary();
  /* create the queue which size can contains 5 elements of Data */
  xQueue = xQueueCreate(5, sizeof(datasensors));
  xTaskCreatePinnedToCore(
      sendTask1,           /* Task function. */
      "sendTask1",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      sendTask2,           /* Task function. */
      "sendTask2",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,1);                    /* Task handle to keep track of created task */ 
}
//////////////////////////////////////////////////////////////////////////////
void loop()
{
  //Send message to receiver
  //const char text[] = "Hello World";
  //Serial.println("send ...");
  //radio.write(&text, sizeof(text));
  
  //delay(1000);
}
///////////////////////////////////////////////////////////////////////////////
void sendTask1( void * parameter )//ultrasonico
{
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data1;
  /* sender 1 has id is 1 */
  data1.sender = 1;

  for(;;){

    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    long t;
    long d;

  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trigger, LOW);

  t = pulseIn(Echo, HIGH);
  d = t/58.3;


    
    datasensors[0]=(d);
    //Serial.println("sendTask1 is sending data");
    /* send data to front of the queue */
    Serial.println("\t\t\t\t ULTRASONICO - Adding "+String(datasensors[0]));
    
    

    
    
    xStatus = xQueueSendToBack( xQueue, &datasensors, xTicksToWait );
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
/* this task is similar to sendTask1 */
void sendTask2( void * parameter )  // temperatura - humedad
{
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data2;
  data2.sender = 2;

  xSemaphoreGive(xBinarySemaphore);
  
  for(;;){
    //Lecturas del sensor
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    datasensors[1]= dht.readHumidity();
    //Serial.println("sendTask2 is sending data");
    Serial.println("\t\t\t\t HUMEDAD - Adding "+String(datasensors[1]));
    //float text1 = data.datasensor;
    //radio.write(&text1, sizeof(text1));
    
    
    //data.datasensor = dht.readTemperature ();
    //Serial.println("\t\t\t\t TEMPERATURA - Adding "+String(data.datasensor));
    //float text2 = data.datasensor2;
    //radio.write(&text2, sizeof(text2));
    
    xStatus = xQueueSendToBack( xQueue, &datasensors, xTicksToWait );

   /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
void receiveTask( void * parameter )
{
  
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  for(;;){
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &datasensors, xTicksToWait );
    /* check whether receiving is ok or not */

    if(xStatus == pdPASS){

      client.loop();
      /* print the data to terminal */
      //Serial.print("Visualizing data: ");
      Serial.print("Sensor 1 = ");
      Serial.print(datasensors[0]);
      //Serial.print(data.sender);
       Serial.print("Sensor 2 = ");
      Serial.print(datasensors[1]);
      sprintf(valores0,"ULTRASONICO = %f cm",datasensors[0]);
      client.publish("python/GRUPO2", valores0);
      delay(500);
      sprintf(valores1,"HUMEDAD = %f",datasensors[1]);
      client.publish("python/GRUPO2", valores1);
      delay(500);

//      radio.write(&datasensors, sizeof(datasensors));
    }
  }
  vTaskDelete( NULL );
}
